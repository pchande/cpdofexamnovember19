package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
@Test
     public void testMinMax1() throws Exception {
     
     int cnumber= new MinMax().function1(4,3);
     assertEquals("Cnumber", 4, cnumber); 
     }
@Test
     public void testMinMax2() throws Exception {
     
     int cnumber= new MinMax().function1(1,4);
     assertEquals("Cnumber", 4, cnumber); 
     }
}
